FROM ubuntu:focal
ENV TZ=Europe/Berlin
ENV DEBIAN_FRONTEND=noninteractive

###############################################################################
#### ----                 Installation Directories                    ---- ####
###############################################################################

ENV INSTALL_DIR=${INSTALL_DIR:-/usr}
ENV SCRIPT_DIR=${SCRIPT_DIR:-$INSTALL_DIR/scripts}

###############################################################################
#### ----          Update Ubuntu / Install Basic Libraries            ---- ####
###############################################################################

ARG BASIC_LIBS="locales gosu ca-certificates"
ARG COMMON_TOOLS="graphviz wget bzip2 bash"

RUN apt-get update -y && \
    apt-get install -y --no-install-recommends ${BASIC_LIBS} && \
    apt-get install -y --no-install-recommends ${COMMON_TOOLS} && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    locale-gen en_US.utf8 de_DE.utf8  

# ###############################################################################
# #### ----                Install Python3 & Conda & Mamba              ---- ####
# ###############################################################################

ENV PATH="/root/miniconda3/bin:$PATH"
ARG PATH="/root/miniconda3/bin:$PATH"

# ###############################################################################
# #### ----                    Install Python Packages                  ---- ####
# ###############################################################################

COPY environment.yml /tmp/env.yaml
# also see https://github.com/mamba-org/mamba#micromamba
RUN wget -qO-  https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
RUN ./bin/micromamba shell init -s bash -p ~/micromamba
CMD source /root/.bashrc
# RUN /bin/bash -c 'source ~/.bashrc' 
CMD micromamba install -y -f /tmp/env.yaml
CMD micromamba clean --all --yes

EXPOSE 9999

RUN groupadd -g 999 webserver && \
    useradd -r -u 999 -g webserver webserver

USER webserver

COPY run.sh /tmp/run.sh
CMD ["bash", "/tmp/run.sh"]
